;; function to move n steps

(define (call-n-times function n)
  ;; call the function n times
  (if (<= n 0)
      #t
      (begin
	(function)
	(call-n-times function (1- n)))))

(define (t-forward n)
  ;; move tortoise n steps forward
  (call-n-times tortoise-move n))

;; function to draw a polygon

(define (polygon num-of-sides length)
  ;;draw a polygon with `num-of-sides` sides and each edge of of size `length`
  (let ((degree (/ 360 num-of-sides)))
    (call-n-times
     (lambda ()
       (t-forward length)
       (tortoise-turn degree))
     num-of-sides)))


;; actual code

(tortoise-pendown)  ;; start drawing


;; draw 5 pentagons
(call-n-times (lambda ()
		(polygon 6 15)
		(sleep 1)
		(tortoise-turn 45)) 8)


(sleep 100)
