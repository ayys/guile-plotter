;; defining some directional functions

;; repeat functions n times
(define (do-n-times function n)
  (if (> n 0)
      (begin
	(function)
	(do-n-times function (- n 1)))
      #t))

(define (move-forward)
  (tortoise-move))

(define (move-backward)
  (tortoise-turn 180)
  (tortoise-move))

(define (turn-left)
  (tortoise-turn 90))

(define (move-right)
  (tortoise-turn (- 90)))


(define (start)
  ;; make sure that these things are done at startup
  (tortoise-pendown)
  (tortoise-reset))



;;; actual program

(start)

(define (draw-square size)
  (do-n-times move-forward size)
  (turn-left)
  (do-n-times move-forward size)
  (turn-left)
  (do-n-times move-forward size)
  (turn-left)
  (do-n-times move-forward size))


(draw-square 30)

(sleep 10)
