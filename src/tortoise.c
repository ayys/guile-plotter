#include <math.h>
#include "src/tortoise.h"

 void
draw_line (FILE* output, double x1, double y1, double x2, double y2)
{
  fprintf (output, "plot [0:1] %f + %f * t, %f + %f * t notitle\n",
           x1, x2 - x1, y1, y2 - y1);
  fflush (output);
}

 SCM
tortoise_reset ()
{
  x = y = 0.0;
  direction = 0.0;
  pendown = 1;

  fprintf (global_output, "clear\n");
  fflush (global_output);
  return SCM_UNSPECIFIED;
}

 SCM
tortoise_pendown ()
{
  SCM result = scm_from_bool(pendown);
  pendown = 1;
  return result;
}

 SCM
tortoise_penup ()
{
  SCM result = scm_from_bool(pendown);
  pendown = 0;
  return result;
}

 SCM
tortoise_turn (SCM scm_degrees)
{
  const double degrees = scm_to_double (scm_degrees);
  direction += M_PI / 180.0 * degrees;
  return SCM_UNSPECIFIED;
}

 SCM
tortoise_move ()
{
  const double length = 1; // move by 1 unit
  double newX, newY;

  newX = x + length * cos (direction);
  newY = y + length * sin (direction);

  if (pendown)
    draw_line (global_output, x, y, newX, newY);

  x = newX;
  y = newY;
  return scm_list_2 (scm_from_double(x), scm_from_double(y));
}
