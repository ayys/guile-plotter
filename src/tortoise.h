#ifndef TORTOISE_H
#define TORTOISE_H
#include <libguile.h>

extern int HEIGHT;

extern int WIDTH;


double x, y;
double direction;
int pendown;
FILE* global_output;


void draw_line (FILE* output, double x1, double y1, double x2, double y2);

SCM tortoise_reset ();

SCM tortoise_pendown ();

SCM tortoise_penup ();

SCM tortoise_turn (SCM scm_degrees);

SCM tortoise_move ();

#endif
