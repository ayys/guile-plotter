#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>
#include <libguile.h>

#include "src/tortoise.h"

int WIDTH = 100;
int HEIGHT = 100;


static FILE*
start_gnuplot (int *child_pid)
{
  FILE* output;
  int pipes[2];
  pid_t pid;

  pipe(pipes);
  pid = fork();

  output = fdopen(pipes[1], "w");

  if (output == NULL)
    {
      fprintf(stderr, "Could not open pipe for writing.\n");
      exit(1);
    }
  
  /* if the child was created, pass it's value to child_pid as well */
  *child_pid = pid;
  
  if (pid == -1)
    {
      /* error - could not fork */
      exit(1);
    }
  if (!pid) /* if pid == 0 */
    {
      /* child process */
      /* duplicate pipes[0] to STDIN File descriptor called ST */
      close(pipes[1]);
      dup2(pipes[0], STDIN_FILENO); /* move pipes[0] as standard input */ 
      execlp("gnuplot", "gnuplot", NULL); // blocking call
      return NULL; /* not reached */
    }
  else
    {
      /* write to the thread via pipe
	 NOTE: finish each command with newline */
      fprintf (output, "set multiplot\n");
      fprintf (output, "set parametric\n");      
      fprintf (output, "set xrange [-%d:%d]\n", WIDTH, WIDTH);
      fprintf (output, "set yrange [-%d:%d]\n", HEIGHT, HEIGHT);
      fprintf (output, "set size ratio -1\n");
      fprintf (output, "unset xtics\n");
      fprintf (output, "unset ytics\n");
      fflush(output);
      return output;
    }
}


static void*
register_functions (void* data)
{
  scm_c_define_gsubr ("tortoise-reset", 0, 0, 0, &tortoise_reset);
  scm_c_define_gsubr ("tortoise-penup", 0, 0, 0, &tortoise_penup);
  scm_c_define_gsubr ("tortoise-pendown", 0, 0, 0, &tortoise_pendown);
  scm_c_define_gsubr ("tortoise-turn", 1, 0, 0, &tortoise_turn);
  scm_c_define_gsubr ("tortoise-move", 0, 0, 0, &tortoise_move);
  return NULL;
}

int
main(int argc, char* argv[])
{
  int child_pid;
  global_output = start_gnuplot(&child_pid);
  
  scm_with_guile (&register_functions, NULL);
  scm_shell(argc, argv);
  fclose(global_output); /* close the read end of the pipe */
  return EXIT_SUCCESS;
}
